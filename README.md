#### 介绍
![img.png](img.png)


#### 健康检测
![img_1.png](img_1.png)
livenessProbe 存活检测，失败重启
readinessProbe 就绪检测，失败不重启，一直等待，流量不进来

#### 指令
docker build -t registry.cn-hangzhou.aliyuncs.com/lee_devops/k8s-go-demo:v1 .

kubectl describe  pod/go-deployment-846f4cb9f7-5gz7c -n dev

kubectl logs -f  pod/go-deployment-846f4cb9f7-5gz7c -n dev -c go