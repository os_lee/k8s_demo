package conf

import (
	"fmt"
	"github.com/spf13/viper"
)

var (
	CF Config
)

type Config struct {
	Service Service
	Secret  Secret
	Etcd    Etcd
}

type Etcd struct {
	Endpoints []string
}

type Service struct {
	Name    string
	Version string
}

type Secret struct {
	Passwd string
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}

func Init() {
	v := viper.New()
	v.SetConfigFile("./conf/config.yaml")
	if err := v.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			fmt.Printf("配置文件config.yaml不存在: %v", err)
		} else {
			fmt.Printf("配置文件config.yaml存在,解析失败: %v", err)
		}
	}
	if err := v.Unmarshal(&CF); err != nil {
		fmt.Printf("config.yaml配置序列化失败: %v", err)
	}

	v.SetConfigFile("./conf/secret.yaml")
	if err := v.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			fmt.Printf("配置文件secret.yaml不存在: %v", err)
		} else {
			fmt.Printf("配置文件secret.yaml存在,解析失败: %v", err)
		}
	}
	if err := v.Unmarshal(&CF); err != nil {
		fmt.Printf("secret.yaml配置序列化失败: %v", err)
	}

	fmt.Printf("%v", CF)
}
