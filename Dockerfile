FROM golang:1.19
ADD ./ /go/src/k8s_demo/
WORKDIR /go/src/k8s_demo
RUN go env -w GOPROXY=https://proxy.golang.com.cn,https://goproxy.cn,direct
RUN go mod init lee/k8s_demo
RUN go mod tidy
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o app .

FROM alpine:latest
MAINTAINER lee
WORKDIR /app/
COPY --from=0 /go/src/k8s_demo/app ./
COPY ./conf/config.yaml ./conf/config.yaml
COPY ./conf/secret.yaml ./conf/secret.yaml
EXPOSE 8888
ENTRYPOINT ["./app"]