package main

import (
	"context"
	"github.com/gin-gonic/gin"
	"lee/k8s_demo/conf"
	"lee/k8s_demo/etcd"
	"log"
	"net/http"
	"os"
	"strings"
	"time"
)

var (
	started = time.Now()
)

func main() {
	conf.Init()
	etcd.Init()
	router := gin.Default()

	router.GET("/", func(c *gin.Context) {
		hostname, _ := os.Hostname()
		c.String(http.StatusOK, "name=%s, version=%s, passwd=%s running in pod=%s",
			conf.CF.Service.Name, conf.CF.Service.Version, conf.CF.Secret.Passwd, hostname)
	})

	router.GET("/health", func(c *gin.Context) {
		log.Println("Healthz 健康检查成功")
		c.String(http.StatusOK, "Health 健康检查成功")
	})

	router.GET("/healthz", func(c *gin.Context) {

		duration := time.Now().Sub(started)
		if duration.Seconds() > 20 {
			log.Println("Healthz 健康检查失败")
			c.String(http.StatusInternalServerError, "Healthz 健康检查失败")
		} else {
			log.Println("Healthz 健康检查成功")
			c.String(http.StatusOK, "Health 健康检查成功")
		}

	})

	/**
	service:
	  name: k8s_demo_etcd
	  version: v1.1
	*/
	router.GET("/etcd", func(c *gin.Context) {
		log.Println("test etcd")
		// etcdctl get 获取
		getResponse, err := etcd.CLI.Get(context.TODO(), "/demo")
		if err != nil {
			panic(err.Error())
		}
		rst := make([]string, 0)
		for _, kv := range getResponse.Kvs {
			rst = append(rst, string(kv.Value))
		}
		c.String(http.StatusOK, strings.Join(rst, ","))
	})

	router.Run(":8888")
}
