package etcd

import (
	"fmt"
	clientv3 "go.etcd.io/etcd/client/v3"
	"lee/k8s_demo/conf"
)

var CLI *clientv3.Client

func Init() {
	client3, err := clientv3.New(
		clientv3.Config{
			Endpoints: conf.CF.Etcd.Endpoints,
		},
	)
	if err != nil {
		fmt.Printf("etcd connect err: %s", err.Error())
	}
	CLI = client3
}
